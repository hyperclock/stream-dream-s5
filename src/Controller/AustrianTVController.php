<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AustrianTVController extends AbstractController
{
    /**
     * @Route("/austrian-tv", name="austrian-tv")
     */
    public function austriantv()
    {
        return $this->render('austrian_tv/index.html.twig', [
            'controller_name' => 'AustrianTVController',
        ]);
    }
}
