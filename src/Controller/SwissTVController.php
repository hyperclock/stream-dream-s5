<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SwissTVController extends AbstractController
{
    /**
     * @Route("/swiss-tv", name="swiss-tv")
     */
    public function swisstv()
    {
        return $this->render('swiss_tv/index.html.twig', [
            'controller_name' => 'SwissTVController',
        ]);
    }
}
