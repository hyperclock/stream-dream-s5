<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DeutschesTVController extends AbstractController
{
    /**
     * @Route("/deutsches-tv", name="deutsches-tv")
     */
    public function deutschestv()
    {
        return $this->render('deutsches_tv/index.html.twig', [
            'controller_name' => 'DeutschesTVController',
        ]);
    }
}
